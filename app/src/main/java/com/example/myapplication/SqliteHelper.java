package com.example.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqliteHelper extends SQLiteOpenHelper {

    String CREATE_TABLE = "" + "CREATE TABLE SPEC (" +
            "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
            "LATITUDE TEXT," +
            "LONGITUDE TEXT," +
            "COMMENT TEXT," +
            "ADDRESS TEXT)";

    public SqliteHelper(@Nullable Context context) {
        super(context, "Spec.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS SPEC");
        onCreate(db);
    }

    void insertLocations(String latitude, String longitude, String comment, String address) {
        SQLiteDatabase db = this.getWritableDatabase();
        String insertQuery = "" +
                "INSERT INTO SPEC (LATITUDE ,LONGITUDE ,COMMENT , ADDRESS )" +
                "VALUES ( '" + latitude + "'  , '" + longitude + "' , '" + comment + "','" + address + "' ) ";
        db.execSQL(insertQuery);
        db.close();
    }


}
