package com.example.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DetailsActivity extends AppCompatActivity {

    int GALLERY_REQUEST_CODE = 100;
    EditText editText_comment;
    SqliteHelper sqliteHelper;
    Button pickButton;
    Button saveButton;
    Uri selectedImage;
    LocationDataModel locationDataModel = new LocationDataModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        bind();
        sqliteHelper = new SqliteHelper(this);

    }

    void bind() {
        saveButton = findViewById(R.id.save_button);
        pickButton = findViewById(R.id.pick_button);
        editText_comment = findViewById(R.id.editText_comment);

        pickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImageSelect();

            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setOrder();
                saveOrder();
            }
        });

    }

    public void onImageSelect() {
        Intent imageIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(imageIntent, GALLERY_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            selectedImage = data.getData();
            Toast.makeText(this, "selected", Toast.LENGTH_SHORT).show();

        }


    }

    void setOrder() {

        Intent intent = getIntent();
        double latitude = intent.getDoubleExtra("latitude", 0);
        double longitude = intent.getDoubleExtra("longitude", 0);
        locationDataModel.setLatitude(latitude + "");
        locationDataModel.setLongitude(longitude + "");
        locationDataModel.setComment(editText_comment.getText().toString());
        locationDataModel.setAddress(getRealPath(this, selectedImage));

    }

    void saveOrder() {

        Toast.makeText(DetailsActivity.this, "insertion Confirmed", Toast.LENGTH_SHORT).show();
sqliteHelper.insertLocations(locationDataModel.getLatitude(),locationDataModel.getLongitude(),locationDataModel.getComment(),locationDataModel.getAddress());
    }

    private String getRealPath(Context mContext, Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = mContext.getContentResolver().query(uri, proj, null, null, null);
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } catch (Exception e) {
            Log.e("Tag", "uri Exception" + e.toString());
            return "";
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }


}

